import { Modalidad } from "./Modalidad";
import { Usuario } from "./Usuario";

export class Equipo {
    id_equipo: Int16Array;
    nombre: string;
    modalidad: Modalidad;
    jugadores: Array<Usuario>;

    constructor(id_equipo: Int16Array,
        nombre: string,
        modalidad: Modalidad,
        jugadores: Array<Usuario>) {
        this.id_equipo = id_equipo;
        this.nombre = nombre;
        this.modalidad = modalidad;
        this.jugadores = jugadores
    }
}