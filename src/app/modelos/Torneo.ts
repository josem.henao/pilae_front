import DateTimeFormat = Intl.DateTimeFormat;
import { Modalidad } from "./Modalidad";
import { Usuario } from "./Usuario";
import { Equipo } from "./Equipo";

export class Torneo {
    nombre: string;
    descripcion: string;
    fecha_inicio: any;
    fecha_fin: any;
    premiacion: string;
    genero: string;
    modalidad: Modalidad;
    equipos: Array<Equipo>;
    administradores: Array<Usuario>;

    constructor(nombre: string,
        descripcion: string,
        fecha_inicio: any,
        fecha_fin: any,
        premiacion: string,
        genero: string,
        modalidad: Modalidad,
        equipos: Array<Equipo>,
        administradores: Array<Usuario>) {

        this.nombre = nombre;
        this.descripcion = nombre;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.premiacion = premiacion;
        this.genero = genero;
        this.modalidad = modalidad;
        this.equipos = equipos;
        this.administradores = administradores;
    }
}