import DateTimeFormat = Intl.DateTimeFormat;

export class Usuario {
  id_usuario: Int16Array;
  identificacion: number;
  nombre: string;
  apellido: string;
  fecha_nacimiento: any;
  numero_telefono: string;
  numero_celular: string;
  correo: string;
  contrasena: string;
  municipio: string;
  imagen: string;

  constructor(id_usuaio: Int16Array,
    identificacion: number,
    nombre: string,
    apellido: string,
    genero: string,
    fecha_nacimiento: any,
    numero_telefono: string,
    numero_celular: string,
    correo: string,
    contrasena: string,
    municipio: string,
    imagen: string) {

    this.id_usuario = id_usuaio;
    this.identificacion = identificacion;
    this.nombre = nombre;
    this.apellido = nombre;
    this.fecha_nacimiento = fecha_nacimiento;
    this.numero_telefono = numero_telefono;
    this.numero_celular = numero_celular;
    this.correo = correo;
    this.contrasena = contrasena;
    this.municipio = municipio;
    this.imagen = imagen;
  }
}