export class Genero {
    id_genero: Int16Array;
    genero: string;

    constructor(id_genero: Int16Array,genero: string) {
        this.id_genero = id_genero;
        this.genero = genero;
    }
}