export class Modalidad {
    id_modalidad: Int16Array;
    modalidad: string;
    descripcion: string;

    constructor(id_modalidad: Int16Array, modalidad: string, descripcion: string) {
        this.id_modalidad = id_modalidad;
        this.modalidad = modalidad;
        this.descripcion = descripcion
    }

}
