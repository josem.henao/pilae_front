import { Component,Input,OnChanges, SimpleChange, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators, NgForm} from '@angular/forms';
import { Usuario } from '../modelos/Usuario'
/* import { RegistrarService } from '../registrar.service'; */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
 /*  providers:[RegistrarService] */
})
export class RegisterComponent implements OnInit {
  
  UsuarioForm: FormGroup;
  usuario: Usuario;

 /*  constructor(private registrarService:RegistrarService) {
   } */

  ngOnInit() {

    this.UsuarioForm = new FormGroup({
      identificacion: new FormControl('',[
        Validators.maxLength(15),
        Validators.required
      ]),
      nombre: new FormControl('',[
        Validators.maxLength(35),
        Validators.required
      ]),
      apellido: new FormControl('',[
        
      ]),
      genero: new FormControl('',[
          
      ]),
      fecha_nacimiento: new FormControl('',[

      ]),
      numero_telefono: new FormControl('',[
  
      ]),
      numero_celular: new FormControl('',[
   
      ]),
      correo: new FormControl('',[
        Validators.required,
      ]),
      contrasena: new FormControl('',[
        Validators.required,
      ]),
      contrasena2: new FormControl('',[
        Validators.required,
      ]),
      residencia: new FormControl('',[
      ])
    })
  }

  ngRegister(){
    if( this.UsuarioForm.controls['contrasena'].value != this.UsuarioForm.controls['contrasena2'].value){
      alert("Las contrasenas no coinciden");
      return
    }

    if(this.UsuarioForm.valid){
      this.usuario = new Usuario(null,
        this.UsuarioForm.controls['identificacion'].value,
        this.UsuarioForm.controls['nombre'].value,
        this.UsuarioForm.controls['apellido'].value,
        this.UsuarioForm.controls['genero'].value,
        this.UsuarioForm.controls['fecha_nacimiento'].value,
        this.UsuarioForm.controls['numero_telefono'].value,
        this.UsuarioForm.controls['numero_celular'].value,
        this.UsuarioForm.controls['correo'].value,
        this.UsuarioForm.controls['contrasena'].value,
        this.UsuarioForm.controls['residencia'].value,
        null
      )
/* 
      this.registrarService.registrarUsuario(this.usuario)
        .subscribe( respuesta => {
        alert("Registrado con éxito")
      },(error:Response)=> {
        alert(error.json());
      }); */



    }
    console.log("usuario: "+this.usuario);
  }


  
    

}
