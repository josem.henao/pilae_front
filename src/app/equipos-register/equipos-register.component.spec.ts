import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquiposRegisterComponent } from './equipos-register.component';

describe('EquiposRegisterComponent', () => {
  let component: EquiposRegisterComponent;
  let fixture: ComponentFixture<EquiposRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquiposRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquiposRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
