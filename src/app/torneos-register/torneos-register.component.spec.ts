import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TorneosRegisterComponent } from './torneos-register.component';

describe('TorneosRegisterComponent', () => {
  let component: TorneosRegisterComponent;
  let fixture: ComponentFixture<TorneosRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TorneosRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TorneosRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
