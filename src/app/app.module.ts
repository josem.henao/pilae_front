import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {Routes, RouterModule} from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';

import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import { EquiposComponent } from './equipos/equipos.component';
import { TorneosComponent } from './torneos/torneos.component';
import { RegisterComponent } from './register/register.component';
import { TorneosRegisterComponent } from './torneos-register/torneos-register.component';
import { EquiposRegisterComponent } from './equipos-register/equipos-register.component';

const appRoutes:Routes = [
  { path: '', component: ContentComponent},
  { path: 'equipos', component: EquiposComponent},
  { path: 'torneos', component: TorneosComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'login', component: LoginComponent},
  { path: 'equipos-register', component:EquiposRegisterComponent},
  { path: 'torneos-registrer', component:TorneosRegisterComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    FooterComponent,
    EquiposComponent,
    TorneosComponent,
    RegisterComponent,
    LoginComponent,
    EquiposRegisterComponent,
    TorneosRegisterComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
